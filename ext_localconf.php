<?php
defined('TYPO3_MODE') || die('Access denied.');


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Tuga.Birthday',
	'Video',
	array(
		'Video' => 'index',
	),
	// non-cacheable actions
	array(
		'Video' => 'index',
	)
);
