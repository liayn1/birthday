'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');

var DEST = '../Resources/Public/';

var CSS_SRC = '../Resources/Private/Scss/**/*.scss';

var JS_SRCS = [
	'../Resources/Private/JavaScript/**/*.js'
];

gulp.task('sass', function () {
	gulp.src(CSS_SRC)
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(DEST + 'Css/'));
});

gulp.task('sass-release', function () {
	gulp.src(CSS_SRC)
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(autoprefixer({
				browsers: ['last 2 versions'],
				cascade: false
			}))
		.pipe(gulp.dest(DEST + 'Css/'));
});

gulp.task('js', function () {
	gulp.src(JS_SRCS)
		.pipe(gulp.dest(DEST + 'JavaScript/'));
});

gulp.task('js-release', function () {
	gulp.src(JS_SRCS)
		.pipe(uglify())
		.pipe(gulp.dest(DEST + 'JavaScript/'));
});

gulp.task('release', ['sass-release', 'js-release']);

gulp.task('watch', function() {
	gulp.watch(CSS_SRC, ['sass']);
	gulp.watch(JS_SRCS, ['js']);
});

gulp.task('default', ['sass', 'js']);
