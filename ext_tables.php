<?php
defined('TYPO3_MODE') || die('Access denied.');

if (TYPO3_MODE === 'BE') {
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Tuga.Birthday',
		'help',
		'tx_birthday',
		'top',
		array(
			'Video' => 'backend',
		),
		array(
			'access' => 'user,group',
			'icon' => 'EXT:birthday/Resources/Public/Icons/youtube.svg',
			'labels' => 'LLL:EXT:birthday/Resources/Private/Language/locallang.xlf'
		)
	);
}
