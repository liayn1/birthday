<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'TUGA Birthday',
	'description' => 'TUGA Birthday extension.',
	'category' => 'plugin',
	'author' => 'Markus Klein',
	'author_email' => 'markus.klein@typo3.org',
	'author_company' => 'Reelworx GmbH',
	'state' => 'stable',
	'uploadfolder' => 0,
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.5.0-7.6.99'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
