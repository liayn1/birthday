

define(['jquery', 'TYPO3/CMS/Core/Modal'], function($, Modal) {
	"use strict";


	var storeVideo = function(videoId) {
		$.ajax({
			url: TYPO3.settings.ajaxUrls['video_preview'],
			data: {videoId: videoId},
			method: 'GET',
			success: function(data) {
				$('#previewImage').find('> img').attr('src', data.previewUrl);
			}
		});
	};

	$(function() {
		$('#videoform').on('submit', function(e) {
			e.preventDefault();
			var videoId = $('#videoId').val();
			if (videoId) {
				storeVideo(videoId);
			}
		});
	});

});