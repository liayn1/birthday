<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Birthday',
	'Video',
	'Video'
);

// hide unused fields for this plugin
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['birthday_video'] = 'layout,select_key,pages,recursive';
