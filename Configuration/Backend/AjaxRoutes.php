<?php
return [
	'video_preview' => [
		'route' => '/video/preview',
		'target' => \Tuga\Birthday\Controller\VideoController::class . '::previewImage'
	]
];