<?php
namespace Tuga\Birthday\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class VideoController extends ActionController {

	public function indexAction() {
		$path = ExtensionManagementUtility::siteRelPath('birthday') . 'Resources/Public/Css/main.css';
		GeneralUtility::makeInstance(PageRenderer::class)->addCssFile($path);

		$registry = GeneralUtility::makeInstance(Registry::class);
		$videoId = $registry->get('birthday', 'video');
		$this->view->assign('videoId', $videoId);
	}

	public function backendAction() {
		$registry = GeneralUtility::makeInstance(Registry::class);
		$videoId = $registry->get('birthday', 'video');

		$this->view->assign('videoId', $videoId);
		$this->view->assign('previewUrl', $this->getPreviewImage($videoId));
	}

	public function previewImage(ServerRequestInterface $request, ResponseInterface $response) {
		$videoId = $request->getQueryParams()['videoId'];

		$registry = GeneralUtility::makeInstance(Registry::class);
		$registry->set('birthday', 'video', $videoId);

		$response->getBody()->write(json_encode([
			'previewUrl' => $this->getPreviewImage($videoId)
		]));
		return $response;
	}

	protected function getPreviewImage($videoId) {
		return 'https://i.ytimg.com/vi/' . $videoId . '/mqdefault.jpg';
	}

}